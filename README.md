# Dev10 Docker Local Server

## Local server on docker compose.
- PHP 7.4.12
- Nginx 1.19.2
- MariaDB 10.5.5
- PhpMyAdmin 5.0.4

## Run Commands
- docker-compose up -d or docker-compose up

## Details
- /etc/hosts edit 127.0.0.1 localhost
- /etc/hosts edit ::1 localhost
- Custom host
- /etc/hosts edit 127.0.0.1 example.com
- /etc/hosts edit ::1 example.com

- http://localhost page details include.
- http://localhost/pma