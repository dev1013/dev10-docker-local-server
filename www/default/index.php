<?php if (($_GET['info'] ?? '') == 'info'): ?>
	<?php phpinfo(); ?>
<?php else: ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head lang="tr" xml:lang="tr">
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Dev10 Local Server</title>
		<style type="text/css">
		    body {
		        font-family: Arial, Helvetica, sans-serif;
		        font-size: .9em;
		        color: #000000;
		        background-color: #FFFFFF;
		        margin: 0;
		        padding: 10px 20px 20px 20px;
		    }
		
		    samp {
		        font-size: 1.3em;
		        font-weight: 600;
		    }
		
		    a {
		        color: #000000;
		        background-color: #FFFFFF;
		    }
		
		    sup a {
		        text-decoration: none;
		    }
		
		    hr {
		        margin-left: 90px;
		        height: 1px;
		        color: #000000;
		        background-color: #000000;
		        border: none;
		    }
		
		    #logo {
		        margin-bottom: 10px;
		        margin-left: 28px;
		    }
		
		    .text {
		        width: 80%;
		        margin-left: 90px;
		        line-height: 140%;
		    }
		</style>
	</head>
	<body>
	    <p><img src="dev10_logo.png" id="logo" alt="Dev10 Logo" width="279" /></p>
	    <p class="text"><strong>The virtual host was set up successfully.</strong></p>
	    <p class="text">
	        Server Name: <samp>localhost</samp><br />
	        Server Software: <samp><?php echo $_SERVER['SERVER_SOFTWARE']; ?></samp><br />
	        Document Root: <samp><?php echo $_SERVER['DOCUMENT_ROOT']; ?></samp><br />
	        PhpMyAdmin: <a href="/pma/" target="_blank">Pma</a><br /><br />
	        Php Version: <samp><?php echo phpversion(); ?></samp><br />
	        Php Info: <a href="index.php?info=info" target="_blank">Info</a><br />
	        Php Internal Network Environment: <samp>INTERNAL_NETWORK - getenv('INTERNAL_NETWORK')</samp><br /><br />
	        
	        Mysql Root Password: <samp>root</samp><br />
	        Mysql Port: <samp>3306</samp><br />
	        Mysql User: <samp>dev10</samp><br />
	        Mysql Password: <samp>dev10</samp><br /><br />
	        
	        Internal Network: <samp><?php echo getenv('INTERNAL_NETWORK'); ?></samp><br /> 
	        <div class="text">
		        Php Extensions: <br />
		        <?php $i = 1; ?>
				<?php foreach (get_loaded_extensions() as $extension): ?>
					<?php echo $i % 5 == 1 ? '<div style="clear:both"></div>' : ''; ?><div style="margin-left:10px;width:100px;float:left"><?php echo $extension; ?></div>
					<?php $i++; ?>
				<?php endforeach; ?>
	        </div>
	    </p>
	</body>
</html>
<?php endif; ?>